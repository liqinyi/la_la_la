import Vue from 'vue'
import App from './App'
import {com} from 'common/common.js'

Vue.config.productionTip = false
Vue.prototype.postApi = com.postApi
Vue.prototype.getApi = com.getApi
Vue.prototype.QQMAPKEY = com.QQMAPKEY
Vue.prototype.setUserInfo = com.setUserInfo
Vue.prototype.getUserInfo = com.getUserInfo
Vue.prototype.upToken = com.upToken
Vue.prototype.getUserProfile = com.getUserProfile
App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
